import hashlib
import time

# Flows status
ALIVE = "ALIVE"
DEAD  = "DEAD"
REPLACED  = "REPLACED"
REBORN = "REBORN"
ALL = "ALL"
DEFUNCT = "DEFUNCT"

def hashme(sa, da, sp, dp, pr, priority, special=1):
    w = str(sa) + str(da) + str(sp) + str(dp) + str(pr) + str(priority) + str(special)
    h = hashlib.md5(w)
    return h.digest().encode('base64')[:6]

class tuple5():
    def __init__(self, sa=None, da=None, sp=None, dp=None, pr=None, iif=None, oif=None, priority=0, moved=False):
        self.sa = sa
        self.da = da
        self.sp = sp
        self.dp = dp
        self.pr = pr
        sa = '-' if not self.sa else str(self.sa)
        da = '-' if not self.da else str(self.da)
        sp = '-' if not self.sp else str(self.sp)
        dp = '-' if not self.dp else str(self.dp)
        pr = '-' if not self.pr else str(self.pr)
        self.hash = hashme(sa, da, sp, dp, pr, priority)
        self.iif = iif
        self.oif = oif
        self.byt = 0
        self.byt_lastreport = 0
        self.brate_lastreport = 0
        self.byt_last_read = 0
        self.pkt = 0
        self.pkt_lastreport = 0
        self.created =  time.time()
        self.updated = self.created
        self.status = ALIVE
        self.duration = 0
        self.moved = moved
        self.priority = priority
        self.warmup_time = self.created
        self.warmup_byt = 0
        self.warmup_pkt = 0
        self.cloned = False

    def dict(self):
        return self.__dict__
