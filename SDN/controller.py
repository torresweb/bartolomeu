from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER, DEAD_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import ipv4
from ryu.lib.packet import tcp
from ryu.lib.packet import udp
from ryu.lib.packet import ether_types
from ryu.ofproto import ether
from ryu.ofproto import inet

from ryu.lib.ovs import vsctl
from operator import attrgetter
from ryu.lib import hub
import tuple5 as t5
import time
import logging
import pandas as pd
import pdb
from collections import deque
import numpy as np
import threading
from subprocess import call
import os
import socket
import random
import sys
pd.options.display.float_format = '{:.7f}'.format

lock_tcpDB = threading.Lock()
lock_output = threading.Lock()
lock_iface_stats = threading.Lock()
lock_flows_stats = threading.Lock()
lock_moved_stats = threading.Lock()
lock_last_best_paths = threading.Lock()
lock_count_move_flows = threading.Lock()
lock_count_output = threading.Lock()

class FlowLoadBalancer(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]
    def __init__(self, *args, **kwargs):
        func_start = time.time()
        super(FlowLoadBalancer, self).__init__(*args, **kwargs)

        # Many tests were perfomed. We read the test_name from a conf_file (create by our scheduler)
        self.hostname = socket.gethostname()
        self.test_name_file = '/tmp/test_name_conf.txt'
        try:
            self.test_name = self.read_test_name()
        except:
            self.test_name = 'no_read_name'

        # pedrovmdmz was used in REAL EXPERIMENT over the Internet.
        # GREtap was used to create VPN
        # To devel and test we used a mininet.
        if self.hostname == "pedrovmdmz":
            self.is_mn = False
            # self.interfaces = ('exp0_l', 'gretap81', 'gretap83', 'gretap82', 'tap80')
            self.interfaces = ('exp0_l', 'gretap81', 'gretap82')
            self.home_dir = "/home/ptorres/experiment1/results/"
            self.reload_script = "/home/ptorres/experiment1/dmz/reload_dmz.sh"
        else:
            self.is_mn = True
            self.interfaces = ('s1-eth1','s1-eth2','s1-eth3','s1-eth4','s1-eth5','s1-eth6')
            # cur_dir = os.getcwd()
            # cur_dir = cur_dir + '/'
            self.home_dir = "/home/torres/shared/simulator"
            if not os.path.exists(self.home_dir):
                os.makedirs(self.home_dir)
            self.reload_script = "/home/torres/mn/reload_controller_mn.sh"
            self.netemvar_script = "/home/torres/mn/do_netem_mn_variable.sh"
            self.netemsta_script = "/home/torres/mn/do_netem_mn_static.sh"

        # Results are dropped here
        self.Results_Dir = self.home_dir + "/results/" + 'batch_paper_lbdm' + "/" + self.test_name + "/"
        if not os.path.exists(self.Results_Dir):
            os.makedirs(self.Results_Dir)

        logfile = self.Results_Dir + './controller.log'
        fh = logging.FileHandler(logfile)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s \n%(message)s')
        fh.setFormatter(formatter)
        self.logger.addHandler(fh)

        # self.logger.setLevel(logging.DEBUG)
        self.logger.setLevel(logging.INFO)

        # Read the alpha, if configured.
        self.alpha_file = '/tmp/alpha_conf.txt'
        try:
            self.alpha_value = self.read_float_value(self.alpha_file)
            if not (0.0 <= self.alpha_value <= 1.0):
                raise
        except:
            self.alpha_value = 0.7
            self.logger.info('##### ALPHA IS NOT VALID!!!! USING ALPHA: %.2f #####', self.alpha_value)

        # Read epsilon, if configured
        self.epsilon_file = '/tmp/epsilon_conf.txt'
        try:
            self.epsilon = self.read_float_value(self.epsilon_file)
            if not (0.0 <= self.epsilon <= 1.0):
                raise
        except:
            self.epsilon = 0.05
            self.logger.info('##### EPSILON IS NOT VALID!!!! USING EPSILON: %.2f #####', self.epsilon)

        # Read q, if configure
        self.quantum_file = '/tmp/quantum_conf.txt'
        try:
            self.quantum = self.read_float_value(self.quantum_file)
            if not (self.quantum > 2.0):
                raise
        except:
            self.quantum = 20.0
            self.logger.info('##### QUANTUM IS NOT VALID!!!! USING QUANTUM: %.2f #####', self.quantum)

        # Choose default assignment method.
        # 0 ECMP Bernoulli
        # 1 WCMP
        # 2 Bfast
        # 3 ECMP Erlang (not used in paper)
        self.assignment_names = ['ECMP Bernoulli', 'WCMP', 'Bfast', 'ECMP Erlang']
        self.default_assignment_file = '/tmp/default_assignment_conf.txt'
        try:
            self.default_assignment = int(self.read_float_value(self.default_assignment_file))
            if not ( 0 <= self.default_assignment <= 3):
                raise
            self.logger.info('##### default_assignment: %d: %s #####', self.default_assignment, self.assignment_names[self.default_assignment])
        except:
            self.default_assignment = 0
            self.logger.info('##### default_assignment IS NOT VALID!!!! USING default_assignment: %d: %s #####', self.default_assignment, self.assignment_names[self.default_assignment])

        self.hist_assig = {}
        # self.default_assignment = 1

        self.logger.debug('##### STARTING __init__ EM %s #####', func_start)

        self.datapath = -1
        # help(self.logger)

        self.iface_internal = self.interfaces[0]
        self.ifARP = self.interfaces[1]

        self.hash_output = {}

        # Special ports used to request things to the controller.
        # 5000: Show TM Stats
        # 5666: Reload
        # 5999: Final Stats
        self.special_ports = [5000, 5666, 5999, 5998, 5997, 5996, 5995, 5994, 5444]
        self.monitor_thread = hub.spawn(self.traffic_monitor)
        self.datapaths = {}

        # Request switch pull stats each N seconds.
        self.timertraffic_monitor = 2
        # Entries with no flows die after X seconds.
        self.timer_tcp_timeout = 20

        # We can operate in many modes to select the egress paths.
        self.modes_ports = range(9001,9018+1)
        self.mode = 1
        # Lista de interfaces em uso
        self.if_load = []
        # It is our B! Only changed if differ accordly to epsilon
        self.if_proportional = []
        # Count change modes.
        self.count_changemode = 0
        # self.seed = 1
        # random.seed(self.seed)
        self.seed = random.randrange(sys.maxsize)
        random.seed(self.seed)
        self.logger.info('##### USING SEED %s #####', self.seed)

        self.select_ifaces(self.count_changemode)
        # Used for Erlang RoundRobin
        self.count_output = 0
        # Activate REBALANCE
        self.doREBALANCE = False

        #interface stats in bytes
        cols = list(self.interfaces)
        cols.append('timestamp')

        self.iface_stats = pd.DataFrame(columns=cols)
        self.iface_stats_file = self.Results_Dir + 'ifacestats_' + str(time.time()) + '_' + str(self.mode) + '.csv'
        self.flows_stats = pd.DataFrame(columns=cols)
        self.flows_stats_file = self.Results_Dir + 'flowsstats_' + str(time.time()) + '_' + str(self.mode) + '.csv'
        cols_moved = ['moved', 'timestamp']
        self.moved_stats = pd.DataFrame(columns=cols_moved)
        self.moved_stats_file = self.Results_Dir + 'movedstats_' + str(time.time()) + '_' + str(self.mode) + '.csv'

        self.last_checked = time.time()
        self.time_last_read = time.time()

        # keep the flows info here
        self.tcpDB = {}
        # find the switch iface id
        OVSDB_ADDR = 'tcp:127.0.0.1:6640'
        ovs_vsctl = vsctl.VSCtl(OVSDB_ADDR)

        self.ifnametoidx = {}
        for ifname in self.interfaces:
            command = vsctl.VSCtlCommand('get', ('Interface', ifname, 'ofport'))
            ovs_vsctl.run_command([command])
            self.ifnametoidx[ifname] = command.result[0][0]
        self.logger.info("Ifaces name:index %s", self.ifnametoidx)
        self.ifidxtoname = {v: k for k, v in self.ifnametoidx.iteritems()}

        # Let path information here
        self.best_queue_size = 500
        self.last_best_paths = deque('', self.best_queue_size)

        # Main Control Loop to take decisions
        self.thread_move_flows = hub.spawn(self.control_loop)

        # Summary of final results here
        self.Results = pd.DataFrame()
        self.ResultsFile = self.Results_Dir + 'results_' + self.hostname + str(time.time()) + '.csv'

        self.udptrackport = 0

        self.start_stats = time.time()
        self.end_stats = self.start_stats
        self.warmup_rcv = time.time()

        self.logger.info('##### USING TEST_NAME %s #####', self.test_name)
        self.logger.info('##### USING ALPHA %.2f #####', self.alpha_value)
        self.logger.info('##### USING EPSILON %.2f #####', self.epsilon)
        self.logger.info('##### USING Q %.2f #####', self.quantum)
        self.logger.info('##### USING DEFAULT ASSIGNMENT %s #####', self.assignment_names[self.default_assignment])

        self.logger.debug('##### FINISHING __init__ EM %.5f #####', time.time() - func_start)

    def control_loop(self):
        func_start = time.time()
        self.logger.debug('##### STARTING control_loop EM %s #####', func_start)
        hub.sleep(self.quantum)
        while True:
            self.logger.debug("Find best path...")
            self.path_information()
            self.logger.info('Calling REBALANCE...')
            self.REBALANCE()
            hub.sleep(self.quantum)
            self.logger.debug('##### SLEEPING control_loop %.5f #####', time.time())

    def path_information(self):
        func_start = time.time()
        self.logger.debug('##### STARTING path_information EM %s #####', func_start)
        df = self.tcpDB_to_df()
        current_read = time.time()
        last_read = self.time_last_read
        self.time_last_read = current_read
        interval_read = current_read - last_read
        # Comeca com zero em todas as interfaces deste modo...
        if_load = pd.Series(0.0,index=self.if_load,name='oif')
        time_max_updated = time.time()
        available_flows = True
        try:
            aval_df = df[(df.status == t5.ALIVE) | ((df.updated > self.last_checked) & (df.status == t5.DEAD))].copy()
            aval_df['byt_counter'] = aval_df.byt - aval_df.byt_last_read
            # if_brate  = aval_df.groupby(['oif']).brate_lastreport.agg("sum")
            if_brate  = aval_df.groupby(['oif']).byt_counter.agg("sum") / interval_read

            # Vamos alterar o valor em tcpDB para marcar a ultima leitura de bytes lidos:
            for ah, ar in aval_df.iterrows():
                self.update_tcpDB(ah, byt_last_read=ar.byt)

            if_flows = aval_df.groupby(['oif']).oif.count()
            # if len(df[(df.status == t5.DEAD)]) > 3:
            #     import pdb; pdb.set_trace()
            if len(if_flows) == 0:
                raise
            # else:
            #     import pdb; pdb.set_trace()
            time_max_updated = aval_df.updated.max()
            self.logger.debug("##### IFBRATE #####\n%s", if_brate)
            self.logger.debug("##### IFFLOWS #####\n%s", if_flows)
            self.logger.debug("##### time_mix_updated #####: ###### %s ###### ", time_max_updated)
        except:
            self.logger.info("No AVAILABLE Flows for stats:\n%s", if_load)
            # Coloca uma entrada vazia no historico...
            if_brate = 0
            if_flows = 0
            available_flows = False

        if_brate = if_load + if_brate
        if_brate.fillna(value=0.0, inplace=True)

        if_flows = if_load + if_flows
        if_flows.fillna(value=0.0, inplace=True)

        # Guardar stats das interfaces (bytes)
        if_stats = if_brate.to_dict()
        # Alterar para utilizar o timestamp do last updated....
        if_stats['timestamp'] = time_max_updated
        for k, v in if_stats.iteritems():
            if_stats[k] = [v]
        df_stat = pd.DataFrame(if_stats)
        with lock_iface_stats:
            self.iface_stats = self.iface_stats.append(df_stat,sort=False)
        self.logger.debug("##### IFACE BYTES STATS #####\n%s", self.iface_stats)

        # Guardar stats das interfaces (flows)
        fl_stats = if_flows.to_dict()
        fl_stats['timestamp'] = time_max_updated
        for k, v in fl_stats.iteritems():
            fl_stats[k] = [v]
        df_fl_stat = pd.DataFrame(fl_stats)
        with lock_flows_stats:
            self.flows_stats = self.flows_stats.append(df_fl_stat,sort=False)
        self.logger.debug("##### IFACE FLOW STATS #####\n%s", self.flows_stats)


        if available_flows:
            kbps_real = (if_brate * 8 / 1000.0)
            no_brate = if_brate[if_brate.lt(100)]
            _teste = 0
            qlen = len(self.last_best_paths)
            # Percorre as interfaces with no_flows
            for nf_i, nf_v in no_brate.iteritems():
                _teste = 1
                try:
                    last_brate = 0
                    count_last = 0.0
                    count_brate = 0
                    # Pecorre o deque de traz pra frente... VER SE QUER PARAR ANTES!
                    for i in range(qlen-1,-1,-1):
                        brate_old = self.last_best_paths[i][nf_i]
                        if brate_old > 0:
                            count_last += 1
                            count_brate += brate_old
                    try:
                        last_brate = count_brate / count_last
                    except:
                        pass
                    if last_brate > 0:
                        if_brate[nf_i] = int(last_brate)
                        self.logger.info("### Iface %s with no flows. Using the MEAN last_brate: %s", nf_i, last_brate)
                        self.logger.debug("#### LAST BEST EXITS ####  %s", self.last_best_paths)
                        # break
                except:
                    pass

            # Divide pelo tempo do traffic monitor!!!
            kbps_used = (if_brate * 8 / 1000.0)
            with lock_last_best_paths:
                self.last_best_paths.append(if_brate)
            self.logger.info("### Oper_Mode: %s Strategy: %s ###", self.mode, self.doREBALANCE)
            self.logger.info("1-Currently real Kbps per output interface\n%s", kbps_real)
            self.logger.info("2-Currently used Kbps per output interface\n%s", kbps_used)
            self.logger.info("3-Currently Flows per output interface\n%s", if_flows)
            self.logger.info("4-Currently real Total Kbps: %s", kbps_real.sum())
            self.logger.info("5-Currently Total Flows: %s", if_flows.sum())
            # self.logger.info("Currently the best is %s", self.last_best_paths)

        self.last_checked = time_max_updated
        self.end_stats = time.time()
        self.logger.debug('##### FINISHING path_information EM %.5f #####', time.time() - func_start)
        return

    def REBALANCE(self):
        func_start = time.time()
        self.logger.debug('##### STARTING REBALANCE EM %s #####', func_start)

        # Verify if we have path information
        qlen = len(self.last_best_paths)
        if qlen < 1:
            self.logger.info("No data in last_best_paths")
            return

        # Just a normalization to let the things simple...
        total_global = 0
        for i in range(0,qlen):
            total_global += self.last_best_paths[i].sum()
        if total_global:
            last_best_paths_norm = []
            for i in range(0,qlen):
                last_best_paths_norm.append(self.last_best_paths[i] / total_global)
        else:
            self.logger.info("No Bytes in history...")
            return

        # EMA
        if_target_load = pd.Series(0.0, index=self.if_load, name='oif')
        for iface in self.if_load:
            list_values = []
            for i in range(0,qlen):
                list_values.append(last_best_paths_norm[i][iface])
            ser_if = pd.Series(list_values)
            if_target_load[iface] = self.exponential_smoothing(ser_if, self.alpha_value)

        # Normalize after EMA
        sum_load = if_target_load.sum()
        if_target_load = if_target_load / sum_load
        if_target_load.fillna(value=0.0, inplace=True)

        # EPSILON
        keep = True
        for ii, iv in if_target_load.iteritems():
            self.logger.info("Iface %s differ in %.6f and epsilon is %.2f", ii, abs(iv - self.if_proportional[ii]), self.epsilon)
            if iv > 0:
                if (abs(iv - self.if_proportional[ii]) > self.epsilon):
                    self.logger.info("!!!!!!!!!Vamos alterar if_proportional se tiver load nas interfaces!!!!!!!!")
                    keep = False
            else:
                self.logger.info("!!!!!!!!!Iface sem carga, if_proportional sera mantido!!!!!!!!")
                keep = True
                break
        if not keep:
            for idx, val in if_target_load.iteritems():
                self.if_proportional[idx] = val
            self.logger.info("####### NEW if_proportional (B)\n%s######", self.if_proportional)
        else:
            if_target_load = self.if_proportional
            self.logger.info("####### USING THE OLD PROPORTIONAL #########\n%s", if_target_load * 100)

        # Do not perform REBALANCE (for WCMP only)
        if not self.doREBALANCE:
            self.logger.info("####### RETURNING AFTER ADJUST if_proportional #########")
            return

        self.logger.info("####### TARGET BALANCE (percent per iface) #########\n%s", if_target_load * 100)

        # Verifify amount of flowa
        df = self.tcpDB_to_df()
        if len(df) == 0:
            self.logger.info("No flows in move_flows")
            return
        df = df[(df.status == t5.ALIVE)]
        if len(df) == 0:
            self.logger.info("No ALIVE flows in move_flows")
            return

        if_load_dict = {}
        if_current_load = if_target_load * 0.0
        flows_to_assign = 0.0
        # Sum the load in each egress and acc in flows_to_assign
        for i in self.if_load:
            try:
                if_load_dict[i] = df[(df.oif == i)]
                load = len(if_load_dict[i]) * 1.0
                if_current_load[i] = load
                flows_to_assign += load
            except:
                if_load_dict[i] = None
                self.logger.info("No flows ALIVE in interface %s", i)

        # Order nexthops by bitrate
        if_target_load.sort_values(ascending=False, inplace=True)
        if_new_load = (if_target_load * 0) # NumberFlowsAssigned
        for i, v in if_target_load.iteritems():
            if_new_load[i] = 1 if (flows_to_assign > 0) else 0
            flows_to_assign -= 1

        if (flows_to_assign >= 1):
            quote = if_target_load.sum() / flows_to_assign
            bitrate_per_quote = if_target_load / quote
            floor = bitrate_per_quote.astype(int)
            remainder = bitrate_per_quote - floor
            if_new_load += floor
            flows_to_assign -= floor.sum()
            # Quem estiver com maior remainder vai ganhar o fluxo.
            # Se tiver empate deveria desempater utilizando a sainda com menor carga primeiro: TODO
            remainder.sort_values(ascending=False, inplace=True)

        i = 0
        while (flows_to_assign > 0):
            nh = remainder.index[i]
            if_new_load[nh] += 1
            flows_to_assign -= 1
            i += 1

        try:
            if if_new_load.sum() != if_current_load.sum():
                raise
        except:
            self.logger.info("Rebalancing is not equal! %s %s",  if_current_load.sum(), if_new_load.sum())
            return

        # Compute de Balance
        self.logger.info("####### CURRENT BALANCE (flows) #########\n%s", if_current_load)
        self.logger.info("####### DESIRED BALANCE (flows) #########\n%s", if_new_load)

        if_balance = if_new_load - if_current_load
        if_balance.fillna(value=0.0, inplace=True)
        self.logger.info("####### PROJECT BALANCE #########\n%s", if_balance)
        # paths with too much flows
        if_debit = if_balance[(if_balance < 0)]

        # df with flows to be debited
        df_debit = df[(df.oif == '__foo__')]
        for iface, flows in if_debit.to_dict().iteritems():
            flows = abs(int(flows))
            self.logger.info("Debit iface %s flows %s",iface, flows)
            df_if = df[(df.oif == iface)]
            # Choose the oldest
            df_partial = df_if.sort_values(by=['duration']).tail(flows)
            df_debit = df_debit.append(df_partial)

        if_credit = if_balance[(if_balance > 0)]
        for new_output, flows_tomove  in if_credit.to_dict().iteritems():
            flows_tomove = abs(int(flows_tomove))
            self.logger.info("Credit iface %s flows %s", new_output, flows_tomove)
            df_tomove = df_debit.head(flows_tomove)
            df_debit = df_debit[flows_tomove:]
            self.logger.info("###################################################")
            self.logger.info("LETS MOVE SOME FLOWS!!! %s", flows_tomove)
            self.logger.info("###################################################")

            # Save stats of moved flows for later...
            mv_stats = {}
            mv_stats['timestamp'] = [time.time()]
            mv_stats['moved'] = [flows_tomove]
            df_mv_stat = pd.DataFrame(mv_stats)
            with lock_moved_stats:
                self.moved_stats = self.moved_stats.append(df_mv_stat,sort=False)
            self.logger.debug("##### IFACE MOVED STATS #####\n%s", self.moved_stats)

            # Request the controller new entries
            try:
                parser = self.datapath.ofproto_parser
                datapath = self.datapath
                self.logger.debug("DATAPATH ID IN MOVE FLOWS: %s", self.datapath.id)
            except:
                self.logger.info("ERROR with datapath")
                return

            for h in df_tomove.index:
                try:
                    self.update_tcpDB(h, status=t5.REPLACED)
                except:
                    self.logger.info("TOMOVE REPLACING FLOW - KEY NOT FOUND: %s", h)
                new_priority = self.tcpDB[h].priority + 1
                iif = self.tcpDB[h].iif
                in_port = self.ifnametoidx[iif]
                sa = self.tcpDB[h].sa
                da = self.tcpDB[h].da
                sp = self.tcpDB[h].sp
                dp = self.tcpDB[h].dp
                pr = self.tcpDB[h].pr
                match = parser.OFPMatch(in_port=in_port,
                                        eth_type=ether.ETH_TYPE_IP,
                                        ip_proto=inet.IPPROTO_TCP,
                                        ipv4_src=sa,
                                        ipv4_dst=da,
                                        tcp_src=sp,
                                        tcp_dst=dp)
                new_output_id = self.ifnametoidx[new_output]
                actions = [parser.OFPActionOutput(new_output_id)]
                self.add_flow(datapath, new_priority, match, actions, idle_timeout=self.timer_tcp_timeout)
                new_h = t5.hashme(sa, da, sp, dp, pr, new_priority)
                moved = True
                tcp_tuple5 = t5.tuple5(sa, da, sp, dp, pr, iif, new_output, new_priority, moved)
                self.logger.debug('##### TRYING LOCKER2 %s #####', time.time())
                with lock_tcpDB:
                    self.logger.debug('##### LOCKED2 %s #####', time.time())
                    self.tcpDB[new_h] = tcp_tuple5
        self.logger.debug('##### FINISHING REBALANCE EM %.5f #####', time.time() - func_start)
        return

    def traffic_monitor(self):
        func_start = time.time()
        self.logger.debug('##### STARTING traffic_monitor EM %s #####', func_start)
        self.logger.debug("Traffic Monitoring...")
        hub.sleep(self.timertraffic_monitor)
        while True:
            for dp in self.datapaths.values():
                self.tm_request_stats(dp)
            self.logger.debug('##### DORMINDO traffic_monitor EM %.5f #####', time.time())
            hub.sleep(self.timertraffic_monitor)

    def tm_request_stats(self, datapath):
        func_start = time.time()
        self.logger.debug('##### STARTING tm_request_stats EM %s #####', func_start)
        self.logger.debug('Send stats request: %016x', datapath.id)
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        req = parser.OFPFlowStatsRequest(datapath)
        datapath.send_msg(req)
        self.logger.debug('##### FINISHING tm_request_stats EM %.5f #####', time.time() - func_start)
        return

    def select_ifaces(self, change_order=0):
        func_start = time.time()
        self.logger.debug('##### STARTING select_ifaces EM %s #####', func_start)
        if self.mode in (1,2,3,4):
            self.if_load =  [self.interfaces[self.mode]]
        elif self.mode in (5, 10):
            c_order = change_order % 2
            if c_order == 0:
                self.if_load = [self.interfaces[1], self.interfaces[2]]
            else:
                self.if_load = [self.interfaces[2], self.interfaces[1]]
        elif self.mode in (6, 11):
            c_order = change_order % 2
            if c_order == 0:
                self.if_load = [self.interfaces[1], self.interfaces[3]]
            else:
                self.if_load = [self.interfaces[3], self.interfaces[1]]
        elif self.mode in (7, 12):
            c_order = change_order % 2
            if c_order == 0:
                self.if_load = [self.interfaces[1], self.interfaces[4]]
            else:
                self.if_load = [self.interfaces[4], self.interfaces[1]]
        elif self.mode in (8, 13):
            c_order = change_order % 2
            if c_order == 0:
                self.if_load = [self.interfaces[1], self.interfaces[5]]
            else:
                self.if_load = [self.interfaces[5], self.interfaces[1]]
        elif self.mode in (9, 14):
            c_order = change_order % 6
            if c_order == 0:
                self.if_load = [self.interfaces[1], self.interfaces[3], self.interfaces[4]]
            elif c_order == 1:
                self.if_load = [self.interfaces[1], self.interfaces[4], self.interfaces[3]]
            elif c_order == 2:
                self.if_load = [self.interfaces[3], self.interfaces[1], self.interfaces[4]]
            elif c_order == 3:
                self.if_load = [self.interfaces[3], self.interfaces[4], self.interfaces[1]]
            elif c_order == 4:
                self.if_load = [self.interfaces[4], self.interfaces[1], self.interfaces[3]]
            else:
                self.if_load = [self.interfaces[4], self.interfaces[3], self.interfaces[1]]
        else:
            self.if_load = self.interfaces

        self.if_proportional = pd.Series(1. / len(self.if_load),index=self.if_load,name='oif')
        for i in self.if_load:
            self.hist_assig[i] = 0

        # import pdb; pdb.set_trace()

        self.logger.info('##### SELECTED IFACES:\n%s', self.if_load)
        self.logger.info('##### PROPORTIONAL IFACES:\n%s', self.if_proportional * 100.)
        self.logger.debug('##### FINISHING select_ifaces EM %.5f #####', time.time() - func_start)
        return

    def exponential_smoothing(self, ser, alpha_value):
        func_start = time.time()
        self.logger.debug('##### STARTING exponential_smoothing EM %s #####', func_start)
        ouput=sum([alpha_value * (1 - alpha_value) ** i * x for i, x in  enumerate(reversed(ser))])
        self.logger.debug('##### FINISHING exponential_smoothing EM %.5f #####', time.time() - func_start)
        return ouput

    def update_tcpDB(self, h, **kwargs):
        func_start = time.time()
        self.logger.debug('##### STARTING update_tcpDB EM %s #####', func_start)
        try:
            _ = self.tcpDB[h]
        except:
            self.logger.info("Hash nao esta mais em tcpDB!!! %s", h)
            return
        if kwargs is not None:
            for k, v in kwargs.iteritems():
                has_attr = True
                try:
                    _v = getattr(self.tcpDB[h], k)
                except:
                    has_attr = False
                    self.logger.info("Atributo nao conhecido %s == %s", k, v)
                if has_attr:
                    with lock_tcpDB:
                        setattr(self.tcpDB[h], k, v)
        self.logger.debug('##### FINISHING update_tcpDB EM %.5f #####', time.time() - func_start)
        return

    @set_ev_cls(ofp_event.EventOFPStateChange, [MAIN_DISPATCHER, DEAD_DISPATCHER])
    def state_change_handler(self, ev):
        func_start = time.time()
        self.logger.debug('##### STARTING state_change_handler EM %s #####', func_start)
        datapath = ev.datapath
        if ev.state == MAIN_DISPATCHER:
            if datapath.id not in self.datapaths:
                self.logger.info('####register datapath: %016x', datapath.id)
                self.datapaths[datapath.id] = datapath
                self.datapath = datapath
                self.logger.info('####DATAPATH ID REGISTERED: %s', self.datapath.id)
                cols = list(self.interfaces)
                cols.append('timestamp')
                with lock_tcpDB:
                    self.tcpDB = {}
                with lock_last_best_paths:
                    self.last_best_paths = deque('', self.best_queue_size)
                with lock_iface_stats:
                    self.iface_stats = pd.DataFrame(columns=cols)
                with lock_flows_stats:
                    self.flows_stats = pd.DataFrame(columns=cols)
                with lock_moved_stats:
                    cols_moved = ['moved', 'timestamp']
                    self.moved_stats = pd.DataFrame(columns=cols_moved)
                with lock_count_move_flows:
                    self.count_move_flows = 0
                with lock_output:
                    self.hash_output = {}
        elif ev.state == DEAD_DISPATCHER:
            if datapath.id in self.datapaths:
                self.logger.info('#### OHHHH NOOOO UNREGISTERED DATAPATH!!!: %016x', datapath.id)
                del self.datapaths[datapath.id]
        else:
            self.logger.info('Received State change to: %s', ev.state)
            return
        self.logger.debug('##### FINISHING state_change_handler EM %.5f #####', time.time() - func_start)
        return

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def switch_features_handler(self, ev):
        func_start = time.time()
        self.logger.debug('##### STARTING switch_features_handler EM %s #####', func_start)
        datapath = ev.msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        # Para o sender
        self.logger.info("Configuring default flow")
        for iface in range(1, len(self.interfaces)):
            in_port = self.ifnametoidx[self.interfaces[iface]]
            out_port = self.ifnametoidx[self.iface_internal]
            priority = 1
            match = parser.OFPMatch(in_port=in_port)
            actions = [parser.OFPActionOutput(out_port)]
            self.add_flow(datapath, priority, match, actions)

        # Do sender (trafego arp e outro nao TCP)
        in_port = self.ifnametoidx[self.iface_internal]
        out_port = self.ifnametoidx[self.ifARP]
        priority = 1
        match = parser.OFPMatch(in_port=in_port)
        actions = [parser.OFPActionOutput(out_port)]
        self.add_flow(datapath, priority, match, actions)
        # Tudo do sender E for TCP envia para o controller
        in_port = self.ifnametoidx[self.iface_internal]
        out_port = ofproto.OFPP_CONTROLLER
        priority = 2
        match = parser.OFPMatch(in_port=in_port, eth_type=ether.ETH_TYPE_IP, ip_proto=inet.IPPROTO_TCP)
        actions = [parser.OFPActionOutput(out_port)]
        self.add_flow(datapath, priority, match, actions)
        # Trick para solicitar dados que estao no controller (enviar algo UDP na porta especial)
        in_port = self.ifnametoidx[self.iface_internal]
        out_port = ofproto.OFPP_CONTROLLER
        actions = [parser.OFPActionOutput(out_port)]
        priority = 2
        for p in self.special_ports:
            match = parser.OFPMatch(eth_type=ether.ETH_TYPE_IP, ip_proto=inet.IPPROTO_UDP, udp_dst=p)
            self.add_flow(datapath, priority, match, actions)
        for p in self.modes_ports:
            match = parser.OFPMatch(eth_type=ether.ETH_TYPE_IP, ip_proto=inet.IPPROTO_UDP, udp_dst=p)
            self.add_flow(datapath, priority, match, actions)
        self.logger.debug('##### FINISHING switch_features_handler EM %.5f #####', time.time() - func_start)
        return

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def packet_in_handler(self, ev):
        func_start = time.time()
        self.logger.debug('##### STARTING packet_in_handler EM %s #####', func_start)
        # If you hit this you might want to increase
        # the "miss_send_length" of your switch
        if ev.msg.msg_len < ev.msg.total_len:
            self.logger.debug("packet truncated: only %s of %s bytes",
                              ev.msg.msg_len, ev.msg.total_len)
        msg = ev.msg
        datapath = msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        in_port = msg.match['in_port']

        pkt = packet.Packet(msg.data)
        eth = pkt.get_protocols(ethernet.ethernet)[0]

        is_tcp = False
        is_udp = False
        if eth.ethertype == ether.ETH_TYPE_IP:
            self.logger.debug("PACKETIN dp:%s in:%s src_eth:%s dst_eth:%s", datapath.id, in_port, eth.src, eth.dst)
            pkt_ipv4 = pkt.get_protocols(ipv4.ipv4)[0]
            if pkt_ipv4.proto == inet.IPPROTO_TCP:
                is_tcp = True
            elif pkt_ipv4.proto == inet.IPPROTO_UDP:
                is_udp = True
            else:
                self.logger.info("Sorry, it is not still implemented...")
                return
        else:
            self.logger.info("Sorry, it is not still implemented...")
            self.logger.debug("packet in dp:%s in:%s src_eth:%s dst_eth:%s", datapath.id, in_port, eth.src, eth.dst)
            return

        if is_tcp:
            seg_tcp = pkt.get_protocols(tcp.tcp)[0]

            self.logger.debug("packet TCP in dp:%s in:%s src_eth:%s dst_eth:%s ip_id:%s, src_ip:%s dst_ip:%s src_tcp:%s dst_tcp:%s", datapath.id, in_port, eth.src, eth.dst, pkt_ipv4.identification, pkt_ipv4.src, pkt_ipv4.dst, seg_tcp.src_port, seg_tcp.dst_port)
            sa = pkt_ipv4.src
            da = pkt_ipv4.dst
            sp = seg_tcp.src_port
            dp = seg_tcp.dst_port
            pr = inet.IPPROTO_TCP
            match = parser.OFPMatch(in_port=in_port,
                                    eth_type=ether.ETH_TYPE_IP,
                                    ip_proto=inet.IPPROTO_TCP,
                                    ipv4_src=sa,
                                    ipv4_dst=da,
                                    tcp_src=sp,
                                    tcp_dst=dp)


            priority = ofproto.OFP_DEFAULT_PRIORITY + 2
            h = t5.hashme(sa, da, sp, dp, pr, priority)
            num_exits = len(self.if_load)
            # Se ja foi atribuido uma porta para este fluxo, reaproveita...
            try:
                out_port = self.hash_output[h]
            except:
                if (self.default_assignment == 0): # ECMP de Bernoulli
                    p = random.randint(0, num_exits-1)
                elif (self.default_assignment == 1): # WCMP
                    ran = random.random()
                    if (num_exits == 1):
                        p0_affinity = 1.0
                        p1_affinity = p0_affinity
                    else:
                        p0_affinity = self.if_proportional[0]
                        p1_affinity = p0_affinity + self.if_proportional[1]
                    if (ran <= p0_affinity):
                        p = 0
                    elif (p0_affinity <= ran <= p1_affinity):
                        p = 1
                    else:
                        p = 2
                    self.logger.info("WCMP ran: %s p0_affinity: %s p1_affinity: %s \n%s", ran, p0_affinity, p1_affinity, self.if_proportional)
                elif (self.default_assignment == 2): # Bfast
                    iface_max = self.if_proportional[(self.if_proportional == self.if_proportional.max())].index[0]
                    p = self.if_load.index(iface_max)
                elif (self.default_assignment == 3): # ECMP de Erlang
                    p = self.count_output % num_exits
                    with lock_count_output:
                        self.count_output += 1

                try:
                    out_port = self.ifnametoidx[self.if_load[p]]
                except:
                    import pdb; pdb.set_trace()
                with lock_output:
                    self.hash_output[h] = out_port

                self.hist_assig[self.if_load[p]] += 1
                ass = pd.Series(self.hist_assig)
                ass_rate = ass / ass.sum()
                self.logger.info("ASSIGNMENT RATE PER PORT\n%s", ass_rate)

            self.logger.debug("OUTPUT PORT IS %d", out_port)

            actions = [parser.OFPActionOutput(out_port)]
            if msg.buffer_id != ofproto.OFP_NO_BUFFER:
                self.add_flow(datapath, priority, match, actions, msg.buffer_id, idle_timeout=self.timer_tcp_timeout)
                self.logger.debug("###  OFP_NO_BUFFER ###")
                # return
            else:
                self.add_flow(datapath, priority, match, actions, idle_timeout=self.timer_tcp_timeout)

            data = None
            if msg.buffer_id == ofproto.OFP_NO_BUFFER:
                self.logger.debug("### Enviando pacote de novo para switch... ###")
                data = msg.data
            out = parser.OFPPacketOut(datapath=datapath, buffer_id=msg.buffer_id,
                                      in_port=in_port, actions=actions, data=data)
            was_sent = datapath.send_msg(out)
            if not was_sent:
                self.logger.info("### ATENCAO: TCP_HANDLER MENSAGEM NAO ENVIADA!!! DATAPATH DESCONECTOU? ###")

            # Cria um objeto com o fluxo e coloca em nosso DB
            iif = self.ifidxtoname[in_port]
            oif = self.ifidxtoname[out_port]
            # Flow foi ressuscitado. O que fazer? Por hora pensa que eh novo
            reborn = True
            try:
                _v = self.tcpDB[h]
            except:
                with lock_tcpDB:
                    self.logger.info('##### NEW FLOW ##### %s', h)
                    tcp_tuple5 = t5.tuple5(sa, da, sp, dp, pr, iif, oif, priority)
                    self.tcpDB[h] = tcp_tuple5
                    reborn = False
            if reborn:
                # Se um fluxo renascer ele pode sobrescrever as estatisticas anteriores
                # Isso eh um problema quando o fluxo anterior tem informacoes mais relevantes
                # Para contornar este problema criaremos uma nova entrada em self.tcpDB com um novo hash
                # Nesse novo hash manteremos os dados antigos.
                if self.tcpDB[h].status == t5.DEAD:
                    self.logger.info('##### FLOW REBORN OLD STATUS: %s', self.tcpDB[h].status)

                    now = time.time()
                    new_h = t5.hashme(sa, da, sp, dp, pr, priority, now)
                    self.logger.info('##### FLOW REBORN %s saving OLD as %s #####', h, new_h)
                    # Cria uma nova instancia de tuple5 que servira para manter as informacoes antigas.
                    with lock_tcpDB:
                        self.logger.debug('##### LOCKED REBORN #####')
                        new_tcp_tuple5 = t5.tuple5(sa, da, sp, dp, pr, iif, oif, priority)
                        self.tcpDB[new_h] = new_tcp_tuple5
                    # Agora atualizamos o novo tuple5 com os dados anteriores.
                    self.update_tcpDB(new_h, iif=self.tcpDB[h].iif, oif=self.tcpDB[h].oif, status=t5.DEAD, pkt_lastreport=self.tcpDB[h].pkt_lastreport, brate_lastreport=self.tcpDB[h].brate_lastreport, byt_last_read=self.tcpDB[h].byt_last_read, byt_lastreport=self.tcpDB[h].byt_lastreport, pkt=self.tcpDB[h].pkt, byt=self.tcpDB[h].byt, created=self.tcpDB[h].created, updated=self.tcpDB[h].updated, duration=self.tcpDB[h].duration, moved=self.tcpDB[h].moved, warmup_time=self.tcpDB[h].warmup_time, warmup_byt=self.tcpDB[h].warmup_byt, warmup_pkt=self.tcpDB[h].warmup_pkt, cloned=True)
                    # Por fim, resetamos os dados anteriores para seguir como um novo...
                    try:
                        self.update_tcpDB(h, status=t5.ALIVE, byt=0, brate_lastreport=0, byt_last_read=0, byt_lastreport=0, pkt=0, pkt_lastreport=0, created=now, warmup_time=now, warmup_pkt=0, warmup_byt=0, updated=now, duration=0, moved=False)
                    except:
                        self.logger.info("REBORN - Key Not Found %s", h)

        if is_udp:
            dat_udp = pkt.get_protocols(udp.udp)[0]
            if dat_udp.dst_port == 5000:
                self.logger.info("Received UDP Magic Packet to Show Stats")
                # Cuidado pois esta mostrando stats de DEFUNCT
                self.show_flow_stats()
            elif dat_udp.dst_port == 5444:
                self.logger.info("### Warmup Time Received... ###")
                self.warmup_rcv = time.time()
                for h in self.tcpDB:
                    current_byt = self.tcpDB[h].byt
                    current_pkt = self.tcpDB[h].pkt
                    try:
                        self.update_tcpDB(h, warmup_time=self.warmup_rcv, warmup_byt=current_byt, warmup_pkt=current_pkt)
                    except:
                        self.logger.info("Warmup - KEY NOT FOUND: %s", h)
            elif dat_udp.dst_port == 5666:
                self.logger.info("### Reloading Controller... ###")
                call(["sudo", self.reload_script])
            elif dat_udp.dst_port == 5994:
                self.logger.info("### Clear all stats (except best exists...) ###")
                cols = list(self.interfaces)
                cols.append('timestamp')
                # Reseta historico
                with lock_tcpDB:
                    self.tcpDB = {}
                with lock_iface_stats:
                    self.iface_stats = pd.DataFrame(columns=cols)
                with lock_flows_stats:
                    self.flows_stats = pd.DataFrame(columns=cols)
                with lock_moved_stats:
                    cols_moved = ['moved', 'timestamp']
                    self.moved_stats = pd.DataFrame(columns=cols_moved)
                with lock_count_move_flows:
                    self.count_move_flows = 0
                with lock_output:
                    self.hash_output = {}
                with lock_count_output:
                    self.count_output = 0
            elif dat_udp.dst_port == 5995:
                self.logger.info("### Disabling Strategy... ###")
                self.doREBALANCE = False
            elif dat_udp.dst_port == 5996:
                self.logger.info("### Enabling Strategy... ###")
                if self.mode >= 10:
                    self.logger.info("##### Strategy 1 turned On  for mode %s #####", self.mode)
                    self.doREBALANCE = True
                else:
                    self.logger.info("##### Strategy still off because mode is off_only #####")
            elif dat_udp.dst_port == 5997:
                self.logger.info("### Calling NETEM Static script... ###")
                call(["sudo", self.netemsta_script])
            elif dat_udp.dst_port == 5998:
                self.logger.info("### Calling NETEM Variable script... ###")
                call(["sudo", self.netemvar_script])
            elif dat_udp.dst_port == 5999:
                udp_src_port = str(dat_udp.src_port)
                self.udptrackport = udp_src_port
                self.logger.info("#####Final stats %s #####", udp_src_port)
                self.end_stats = time.time()
                self.save_final_stats()
                self.warmup_rcv = self.end_stats
            elif dat_udp.dst_port >= 9001:
                # # Salva stats das interfaces
                new_mode = dat_udp.dst_port - 9000
                self.logger.info("##### Change Mode to %s", new_mode)
                self.logger.info("##### Disabling Strategy for New Mode... #####")
                self.doREBALANCE = False
                self.mode = new_mode
                self.count_changemode += 1
                self.select_ifaces(self.count_changemode)
                cols = list(self.interfaces)
                cols.append('timestamp')
                # Reseta historico
                with lock_last_best_paths:
                    self.last_best_paths = deque('', self.best_queue_size)
                with lock_iface_stats:
                    self.iface_stats = pd.DataFrame(columns=cols)
                with lock_flows_stats:
                    self.flows_stats = pd.DataFrame(columns=cols)
                with lock_moved_stats:
                    cols_moved = ['moved', 'timestamp']
                    self.moved_stats = pd.DataFrame(columns=cols_moved)
                with lock_count_move_flows:
                    self.count_move_flows = 0
                with lock_output:
                    self.hash_output = {}
                # Isto sera usado para variar a porta de saida do round-robin em cada repeticao
                with lock_count_output:
                    self.count_output = 0

                self.start_stats = time.time()
                self.end_stats = self.start_stats
                self.warmup_rcv = time.time()
            else:
                pass
        self.logger.debug('##### FINISHING packet_in_handler EM %.5f #####', time.time() - func_start)
        return

    @set_ev_cls(ofp_event.EventOFPFlowStatsReply, MAIN_DISPATCHER)
    def stats_reply_handler(self, ev):
        func_start = time.time()
        self.logger.debug('##### STARTING stats_reply_handler EM %s #####', func_start)
        msg = ev.msg
        ofp = msg.datapath.ofproto
        body = ev.msg.body
        self.flow_stats_reply_handler(body)
        self.logger.debug('##### FINISHING stats_reply_handler EM %.5f #####', time.time() - func_start)
        return

    @set_ev_cls(ofp_event.EventOFPFlowRemoved, MAIN_DISPATCHER)
    def flow_removed_handler(self, ev):
        func_start = time.time()
        self.logger.debug('##### STARTING flow_removed_handler EM %s #####', func_start)
        msg = ev.msg
        dp = msg.datapath
        ofp = dp.ofproto

        is_idle_timeout = False
        if msg.reason == ofp.OFPRR_IDLE_TIMEOUT:
            reason = 'IDLE TIMEOUT'
            is_idle_timeout = True
        elif msg.reason == ofp.OFPRR_HARD_TIMEOUT:
            reason = 'HARD TIMEOUT'
        elif msg.reason == ofp.OFPRR_DELETE:
            reason = 'DELETE'
        elif msg.reason == ofp.OFPRR_GROUP_DELETE:
            reason = 'GROUP DELETE'
        else:
            reason = 'unknown'

        self.logger.info('OFPFlowRemoved received: '
                        'cookie=%d priority=%d reason=%s table_id=%d '
                        'duration_sec=%d duration_nsec=%d '
                        'idle_timeout=%d hard_timeout=%d '
                        'packet_count=%d byte_count=%d match.fields=%s',
                        msg.cookie, msg.priority, reason, msg.table_id,
                        msg.duration_sec, msg.duration_nsec,
                        msg.idle_timeout, msg.hard_timeout,
                        msg.packet_count, msg.byte_count, msg.match)
        is_tcp = True
        try:
            sp = msg.match['tcp_src']
            dp = msg.match['tcp_dst']
        except:
            is_tcp = False
         #
        if is_tcp and is_idle_timeout:
            sa = msg.match['ipv4_src']
            da = msg.match['ipv4_dst']
            pr = inet.IPPROTO_TCP
            iif = self.ifidxtoname[msg.match['in_port']]
            byt = msg.byte_count
            pkt = msg.packet_count
            priority = msg.priority
            h = t5.hashme(sa, da, sp, dp, pr, priority)
            # update tcpDB with new pkt and byt values
            duration = msg.duration_sec+(msg.duration_nsec * 1.0  / pow(10,9))
            duration = duration-msg.idle_timeout
            try:
                updated = self.tcpDB[h].created + duration
                inter_duration = updated - self.tcpDB[h].updated
                inter_bytes = byt - self.tcpDB[h].byt
                self.update_tcpDB(h, status=t5.DEAD, pkt_lastreport=pkt-self.tcpDB[h].pkt, brate_lastreport=(inter_bytes/inter_duration), byt_lastreport=inter_bytes, pkt=pkt, byt=byt, updated=updated, duration=duration)
                self.logger.info("DEAD FLOW: %s, %s", h, self.tcpDB[h].dict())
            except:
                self.logger.info("DEAD FLOW NOT FOUND!!!: %s", h)

        self.logger.debug('##### FINISHING flow_removed_handler EM %.5f #####', time.time() - func_start)
        return

    def flow_stats_reply_handler(self, body):
        func_start = time.time()
        self.logger.debug('##### STARTING flow_stats_reply_handler EM %s #####', func_start)
        flows = []
        for stat in body:
            flows.append('table_id=%s '
                        'duration_sec=%d duration_nsec=%d '
                        'priority=%d '
                        'idle_timeout=%d hard_timeout=%d '
                        'cookie=%d packet_count=%d byte_count=%d '
                        'match=%s instructions=%s' %
                        (stat.table_id,
                        stat.duration_sec, stat.duration_nsec,
                        stat.priority,
                        stat.idle_timeout, stat.hard_timeout,
                        stat.cookie, stat.packet_count, stat.byte_count,
                        stat.match, stat.instructions))

            # print stat.instructions
            is_tcp = True
            try:
                sp = stat.match['tcp_src']
                dp = stat.match['tcp_dst']
            except:
                is_tcp = False
                # print stat.match['in_port']

            if is_tcp:
                # print stat.match
                sa = stat.match['ipv4_src']
                da = stat.match['ipv4_dst']
                pr = inet.IPPROTO_TCP
                oif = self.ifidxtoname[stat.instructions[0].actions[0].port]
                iif = self.ifidxtoname[stat.match['in_port']]
                byt = stat.byte_count
                pkt = stat.packet_count
                priority = stat.priority
                # print "############## PRIO!!!", priority
                # self.logger.info("%s->%s: %s:%s %s:%s = %d bytes, %d packets", iif, oif, sa, sp, da, dp, byt, pkt)
                h = t5.hashme(sa, da, sp, dp, pr, priority)
                # update tcpDB with new pkt and byt values
                duration = stat.duration_sec+(stat.duration_nsec * 1.0  / pow(10,9))
                # now = time.time()
                try:
                    updated = self.tcpDB[h].created + duration
                    inter_duration = updated - self.tcpDB[h].updated
                    inter_bytes = byt - self.tcpDB[h].byt
                    self.logger.debug("%s->%s: %d bytes, %d byt_lastreport", iif, oif, byt, byt-self.tcpDB[h].byt)
                    bytes_sent = byt - self.tcpDB[h].byt
                    self.update_tcpDB(h, oif=oif, pkt_lastreport=pkt-self.tcpDB[h].pkt, brate_lastreport=(inter_bytes/inter_duration), byt_lastreport=inter_bytes, pkt=pkt, byt=byt, updated=updated, duration=duration)
                    self.logger.debug("UPDATED %s, %s", h, self.tcpDB[h].dict())
                    self.logger.debug("######### BRATE IS!! ########## %s: %s", h, (inter_bytes/inter_duration))

                except:
                    self.logger.info("UPDATING - KEY NOT FOUND!! %s", h)
        self.logger.debug('##### FINISHING flow_stats_reply_handler EM %.5f #####', time.time() - func_start)
        return

    def add_flow(self, datapath, priority, match, actions, buffer_id=None, idle_timeout=0):
        func_start = time.time()
        self.logger.debug('##### STARTING add_flow EM %s #####', func_start)
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS, actions)]
        if buffer_id:
            mod = parser.OFPFlowMod(datapath=datapath, buffer_id=buffer_id, priority=priority, match=match,
                                    instructions=inst, idle_timeout=idle_timeout, flags=ofproto.OFPFF_SEND_FLOW_REM)
        else:
            mod = parser.OFPFlowMod(datapath=datapath, priority=priority, match=match, instructions=inst, idle_timeout=idle_timeout,
                                    flags=ofproto.OFPFF_SEND_FLOW_REM)
        was_sent = datapath.send_msg(mod)
        if not was_sent:
            self.logger.info("### ATENCAO: ADD-FLOW: MENSAGEM NAO ENVIADA!!! DATAPATH DESCONECTOU? ###")

        self.logger.debug('##### FINISHING add_flow EM %.5f #####', time.time() - func_start)
        return

    def tcpDB_to_df(self):
        func_start = time.time()
        self.logger.debug('##### STARTING tcpDB_to_df EM %s #####', func_start)
        df = pd.DataFrame()
        with lock_tcpDB:
            self.logger.debug('##### TAMANHO de self.tcpDB:  %d #####', len(self.tcpDB))
            loop_start = time.time()
            self.logger.debug('##### LOOP EM tcpDB_to_df EM %s #####', loop_start)
            vals = []
            for t in self.tcpDB:
                d = self.tcpDB[t].dict()
                val = d.values()
                val.append(t)
                vals.append(val)
            try:
                col = d.keys()
            except:
                return df
            col.append('t')
            df = pd.DataFrame(vals, columns=col)
            df.set_index('t', inplace=True)
            self.logger.debug('##### FIM-LOOP tcpDB_to_df EM %.5f #####', time.time() - loop_start)
        self.logger.debug('##### FINISHING tcpDB_to_df EM %.5f de TAMANHO %d #####', time.time() - func_start, len(self.tcpDB))
        return df

    def show_flow_stats(self, status=t5.ALL):
        func_start = time.time()
        self.logger.debug('##### STARTING show_flow_stats EM %s #####', func_start)
        df = self.tcpDB_to_df()
        if status != t5.ALL:
            try:
                df = df[(df.status == status)]
            except:
                pass

        try:
            if len(df) < 1:
                return
            else:
                self.logger.info("##############::%s", status)
        except:
            self.logger.info("DF is Empty")
            return

        self.logger.info("Flows\n %s", df)
        # Show sent bytes/pkt per oif:
        try:
            x = df.groupby(['oif'])
            byt = x.agg(['count', 'min', 'max', 'mean', 'sum']).byt
            pkt = x.agg(['count', 'min', 'max', 'mean', 'sum']).pkt
            self.logger.info("Bytes\n %s", byt)
            self.logger.info("Packets\n %s", pkt)
            # per second...
            created = x.agg(['min']).created
            created = created.rename(columns={"min": "time"})
            lastupdated = x.agg(['max']).updated
            lastupdated = lastupdated.rename(columns={"max": "time"})
            duration = lastupdated - created
            duration = duration['time']
            ifsum = byt['sum']
            kbps = (ifsum * 8 / 1000.0) / duration
            self.logger.info("Kbps per output interface\n %s", kbps)
        except:
            pass
        self.logger.debug('##### FINISHING show_flow_stats EM %.5f #####', time.time() - func_start)

    def calc_idle(self, df_dead_arg):
        func_start = time.time()
        self.logger.debug('##### STARTING calc_idle EM %s #####', func_start)

        df_dead = df_dead_arg.copy()
        df_dead.sort_values(by=['created', 'updated'], inplace=True)
        df_dead.reset_index(inplace=True, drop=True)
        left = min(df_dead.created[0], df_dead.updated[0])
        ival = pd.Interval(left=left, right=df_dead.updated[0], closed='both')
        ival_list = []
        for i, row in df_dead.iterrows():
            if row.created in ival:
                new_end = max(ival.right, row.updated)
                ival = pd.Interval(left=ival.left, right=new_end, closed='both')
            else:
                ival_list.append(ival)
                left = min(row.created, row.updated)
                ival = pd.Interval(left=left, right=row.updated, closed='both')

        ival_list.append(ival)
        idle_duration = 0.0
        for i in range(0, len(ival_list)):
            try:
                idle_duration += ival_list[i+1].left - ival_list[i].right
            except:
                pass
        self.logger.debug('##### FINISHING calc_idle EM %.5f #####', time.time() - func_start)
        return idle_duration

    def save_final_stats(self):
        func_start = time.time()
        self.logger.debug('##### STARTING save_final_stats EM %s #####', func_start)
        self.logger.info("SAVE_FINAL_STATS")

        deads = -1
        all = 0

        df = self.tcpDB_to_df()
        try:
            all = len(df)
        except:
            all = 0

        try:
            df_dead = df[(df.status == t5.DEAD)]
            deads = len(df_dead)
        except:
            self.logger.info("No TCP DEADs...")
            deads = -1
            # return

        if deads < 1:
            self.logger.info("### Final Stats - NO DEAD FLOWS! %s - all: %s", deads, all)
            return
        elif deads < all:
            self.logger.info("ATENCAO, NEM TODOS OS FLUXOS ESTAO MORTOS!!! dead: %s, all: %s", deads, all)
        else:
            self.logger.info("DEAD Flows:%s", deads)

        deadflows_file = self.Results_Dir + 'deadflows_' + str(time.time()) + '_' + str(self.udptrackport) + '.csv'
        df_dead.to_csv(deadflows_file, sep='\t', index=False, header=True, float_format='%.9f')

        # Calcular o idle_duration
        try:
            idle_duration = self.calc_idle(df_dead)
        except:
            self.logger.info("#### ERROR IN IDLE DURATION 1")
            idle_duration = 0
        self.logger.info("#### IDLE DURATION GROSS IS: %s", str(idle_duration))

        start = df_dead.created.min()
        end = df_dead.updated.max()
        total_duration = end - start
        total_duration -= idle_duration
        last_updated = df_dead.updated.max() - start

        moved = len(df_dead[(df_dead.moved == True)])
        flows = len(df_dead)
        g_dead = df_dead.groupby(['sa','da','sp','dp'])
        unique_flows = len(g_dead)
        mean_duration = g_dead.duration.sum().mean()
        std_duration = g_dead.duration.sum().std()
        max_duration = g_dead.duration.sum().max()
        min_duration = g_dead.duration.sum().min()
        # Quantidade de vezes que um fluxo foi movido
        single_moved = g_dead.moved.count().max() - 1

        total_byt = df_dead.byt.sum()
        total_pkt = df_dead.pkt.sum()
        mean_byts = df_dead.byt.mean()
        mean_pkts = df_dead.pkt.mean()

        df_dead_net = df_dead[(df_dead.updated > df_dead.warmup_time)]

        try:
            idle_duration_net = self.calc_idle(df_dead_net)
        except:
            self.logger.info("#### ERROR IN IDLE DURATION NET")
            idle_duration_net = 0

        self.logger.info("#### IDLE DURATION NET IS: %s", str(idle_duration_net))
        start = df_dead_net.created.min()
        end_net = df_dead_net.updated.max()
        warmup_start_net = df_dead_net.warmup_time.min()
        net_duration = end_net - warmup_start_net
        net_duration -= idle_duration_net
        net_total_byt = (df_dead_net.byt - df_dead_net.warmup_byt).sum()
        net_total_pkt = (df_dead_net.pkt - df_dead_net.warmup_pkt).sum()

        self.logger.info("Gross Mean T kbps: %s", (total_byt * 8 / total_duration) / 1000)
        self.logger.info("Net Mean T kbps: %s", (net_total_byt * 8 / net_duration) / 1000)

        oper_mode = self.mode
        if self.doREBALANCE:
            strategy = 1
        else:
            strategy = 0
        udptrackport = self.udptrackport

        alpha = self.alpha_value
        epsilon = self.epsilon
        quantum = self.quantum
        assignment = self.assignment_names[self.default_assignment]
        rebalance = 'on' if (self.doREBALANCE) else 'off'
        save_time = str(time.time())

        res_header =  ('oper_mode', 'save_time', 'strategy', 'alpha', 'epsilon', 'quantum', 'assignment', 'rebalance', 'udptrackport', 'start', 'end', 'idle_duration', 'total_duration', 'mean_duration', 'max_duration', 'min_duration', 'std_duration', 'moved', 'single_moved', 'flows', 'unique_flows', 'total_byt', 'total_pkt', 'mean_byts', 'mean_pkts', 'net_duration', 'net_total_byt', 'net_total_pkt', 'idle_duration_net', 'last_updated')

        results_dict = {}
        for k in res_header:
            results_dict[k] = [eval(k)]

        Rdf = pd.DataFrame.from_dict(results_dict)
        self.Results = self.Results.append(Rdf).reset_index(drop=True)

        self.Results.to_csv(self.ResultsFile, sep='\t', index=False, header=True, float_format='%.9f')

        with lock_tcpDB:
            self.tcpDB = {}

        cols = list(self.interfaces)
        cols.append('timestamp')

        self.logger.info("##### SAVE IFACE STATS FINAL #####\n%s", self.iface_stats)
        self.iface_stats_file = self.Results_Dir + 'ifacestats_' + save_time + '_' + str(self.udptrackport) + '.csv'
        self.iface_stats.to_csv(self.iface_stats_file, sep='\t', index=False, header=True)
        with lock_iface_stats:
            self.iface_stats = pd.DataFrame(columns=cols)

        self.logger.info("##### SAVE FLOWS IFACE STATS FINAL #####\n%s", self.flows_stats)
        self.flows_stats_file = self.Results_Dir + 'flowsstats_' + save_time + '_' + str(self.udptrackport) + '.csv'
        self.flows_stats.to_csv(self.flows_stats_file, sep='\t', index=False, header=True)
        with lock_flows_stats:
            self.flows_stats = pd.DataFrame(columns=cols)

        cols_moved = ['moved', 'timestamp']
        self.logger.info("##### SAVE MOVED STATS FINAL #####\n%s", self.moved_stats)
        self.moved_stats_file = self.Results_Dir + 'movedstats_' + save_time + '_' + str(self.udptrackport) + '.csv'
        self.moved_stats.to_csv(self.moved_stats_file, sep='\t', index=False, header=True)
        with lock_moved_stats:
            self.moved_stats = pd.DataFrame(columns=cols_moved)


        self.logger.debug('##### FINISHING save_final_stats EM %.5f #####', time.time() - func_start)
        return

    def read_test_name(self):
        with open(self.test_name_file) as f:
            content = f.readlines()
        content = [x.strip() for x in content][0]
        return content

    def read_float_value(self, value_file):
        with open(value_file) as f:
            content = f.readlines()
        content = [x.strip() for x in content][0]
        return float(content)
