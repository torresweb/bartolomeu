#!/usr/bin/env python
import random
import simpy
import numpy as np
import pandas as pd
pd.options.display.float_format = '{:.7f}'.format
import math
from timeit import Timer
import time
import sys
import operator

p_busy = {}
p_busy[0] = False
p_busy[1] = False
# Paths's Queues
path0_q = {}
path1_q = {}
sending_q0_proc = False
sending_q1_proc = False
# End of the work
max_finished = 0
total_moved = 0
# Acc for response time
accR = 0
# Acc for waiting
accW = 0
# count sent flows...
finished_flows = 0
# Final report
rep = {}
# Final report to TCAM impact
rep_all_flows = {}
rep_moved = {}
rep_moved_all_flows = {}

PATH_SPEED_P0 = 1
PATH_SPEED_P1 = 1

verbose = True
paths = []

try:
    cpu = int(sys.argv[1])
    print cpu
except:
    print 'arg 1 should be cpu id'
    exit()

try:
    dataset = int(sys.argv[2])
    print dataset
except:
    print 'arg 2 should be 1 for rnp and 2 for wide'
    exit()

if (dataset == 1):
    rnp = True
else:
    rnp = False

def isp_rr_flow(env, flow, path_id, bytes_tosend, paths_resource):
    global PATH_SPEED_P0
    global PATH_SPEED_P1
    global max_finished
    global accR
    global finished_flows
    global rep
    global path0_q
    global path1_q
    global sending_q0_proc
    global sending_q1_proc
    global verbose
    global rep

    # Put each flow in shared queue
    if verbose:
        print('%.4f Flow: flow %s arriving at isp in path %s to send: %.4f B' % (env.now, flow, path_id, bytes_tosend))

    arrived = env.now
    rep[flow] = [arrived, arrived, arrived]

    if path_id == 0:
        path_q = path0_q
        SENDING_SPEED = PATH_SPEED_P0
        with paths_resource[path_id].request() as req:
            yield req
            try:
                sending_q0_proc.interrupt()
                if verbose:
                    print '%.4f Flow:!!!! interrupted - path0 !!! for flow %d' % (env.now, flow)
            except:
                if verbose:
                    print '%.4f Flow: not interrupted - path0 for flow %d' % (env.now, flow)
                pass
            yield env.timeout(0)
            path_q[flow] = [env, arrived, flow, bytes_tosend]
            sending_q0_proc = env.process(sending_rr_flow(env, path_q, SENDING_SPEED))

    else:
        path_q = path1_q
        SENDING_SPEED = PATH_SPEED_P1
        with paths_resource[path_id].request() as req:
            yield req
            try:
                sending_q1_proc.interrupt()
                if verbose:
                    print '%.4f Flow: !!!! interrupted - path1 !!! for flow %d' % (env.now, flow)
            except:
                if verbose:
                    print '%.4f Flow: not interrupted - path1 for flow %d' % (env.now, flow)
                pass
            yield env.timeout(0)
            path_q[flow] = [env, arrived, flow, bytes_tosend]
            sending_q1_proc = env.process(sending_rr_flow(env, path_q, SENDING_SPEED))

def sending_rr_flow(env, path_q, SENDING_SPEED):
    global accR
    global finished_flows
    global max_finished
    global verbose
    global rep

    while True:
        starting_to_send = env.now
        try:
            min_data = path_q.itervalues().next()[3]
            for c in path_q:
                min_data = min(min_data, path_q[c][3])
            time_to_send = (min_data / SENDING_SPEED) * len(path_q)
            yield env.timeout(time_to_send)
            # Update queue
            finished_flows_list = []
            for c in path_q:
                if verbose:
                    print '%4f Send: Update flow %d data: %4f-%4f' % (env.now, c, path_q[c][3], min_data)
                upd_data = path_q[c][3] - min_data
                path_q[c][3] = upd_data
                if upd_data == 0:
                    finished_flows_list.append(c)
            # Remove finisheds
            accR += (env.now - starting_to_send) * len(path_q)
            if verbose:
                print '%.4f Send: accR1: %.4f added: %.4f' % (env.now, accR, (env.now - starting_to_send) * len(path_q))
            for c in finished_flows_list:
                if verbose:
                    print '%.4f Send:  flow Finished %d sent %.4f missing_data %.4f' % (env.now, c, upd_data, path_q[c][3])
                del path_q[c]
                arrived = rep[c][0]
                rep[c] = [arrived, env.now, arrived]
            finished_flows += len(finished_flows_list)
            max_finished = max(max_finished, env.now)
            if len(path_q) == 0:
                return
        except simpy.Interrupt as i:
            interrupt_time = env.now
            # Update the queue with the remaning to send
            sending_time = interrupt_time - starting_to_send
            sent = (sending_time * SENDING_SPEED) / len(path_q)
            # Update
            finished_flows_list = []
            for c in path_q:
                upd_data = path_q[c][3] - sent
                path_q[c][3] = upd_data
                if upd_data == 0:
                    finished_flows_list.append(c)
                accR += sending_time
                if verbose:
                    print '%.4f Send:  flow %d accR2 Interrupted: %.4f %.4f sent %.4f missing_data %.4f' % (env.now, c, accR, sending_time, sent, path_q[c][3])
            # Remove finisheds
            for c in finished_flows_list:
                if verbose:
                    print '%4f Send:  flow %d finished' % (env.now, c)
                accR += (env.now - starting_to_send)
                if verbose:
                    print '%.4f Send:  accR3: %.4f' % (env.now, accR)
                del path_q[c]
                arrived = rep[c][0]
                rep[c] = [arrived, env.now, arrived]
            return

def swap_speed_flow(env, swap_time, paths_resource, total_flows):
    global path0_q
    global path1_q
    global PATH_SPEED_P0
    global PATH_SPEED_P1
    global sending_q0_proc
    global sending_q1_proc
    global verbose
    global finished_flows

    yield env.timeout(0)

    print len(paths_resource)

    while True:
        yield env.timeout(0)
        # Time to finish?
        if finished_flows == total_flows:
            if verbose:
                print env.now, 'Swap: returning finished == total_flows'
            return
        # Interrupt queues
        try:
            with paths_resource[0].request() as req:
                yield req
                if verbose:
                    print '%.4f Swap: Swap going to interrupt path 0' % (env.now)
                sending_q0_proc.interrupt()
        except:
            pass
        try:
            with paths_resource[1].request() as req:
                yield req
                if verbose:
                    print '%.4f Swap: Swap going to interrupt path 1' % (env.now)
                sending_q1_proc.interrupt()
        except:
            pass
        # Swap speeds...
        yield env.timeout(0)
        temp_spd = PATH_SPEED_P0
        PATH_SPEED_P0 = PATH_SPEED_P1
        PATH_SPEED_P1 = temp_spd
        # Restart sending
        with paths_resource[0].request() as req:
            yield req
            if verbose:
                print '%4f Swap: Restarting sending_rr for path 0' % (env.now)
                print path0_q
            sending_q0_proc = env.process(sending_rr_flow(env, path0_q, PATH_SPEED_P0))
        with paths_resource[1].request() as req:
            yield req
            if verbose:
                print '%4f Swap: Restarting sending_rr for path 1' % (env.now)
                print path1_q
            sending_q1_proc = env.process(sending_rr_flow(env, path1_q, PATH_SPEED_P1))
        yield env.timeout(swap_time)  # Check every...

def REBALANCE(env, first_check, quantum, total_flows, paths_resource, test_name='no_name', mode_name='no_mode'):
    global path0_q
    global path1_q
    global PATH_SPEED_P0
    global PATH_SPEED_P1
    global total_moved
    global finished_flows
    global sending_q0_proc
    global sending_q1_proc
    global verbose
    global rep_moved

    yield env.timeout(first_check)
    while True:
        yield env.timeout(0)
        if finished_flows == total_flows:
            if verbose:
                print env.now, 'Mon: returning finished == total_flows'
            return
        rate_p0 = float(PATH_SPEED_P0) / float(PATH_SPEED_P1)
        len_p0 = len(path0_q)
        len_p1 = len(path1_q)

        NumberFlowsAssigned = {}
        NumberFlowsAssigned[0] = 0
        NumberFlowsAssigned[1] = 0
        B = {}
        B[0] = PATH_SPEED_P0
        B[1] = PATH_SPEED_P1
        FlowsToAssing = len_p0 + len_p1
        if FlowsToAssing > 0:
            for n in range(0, 2):
                NumberFlowsAssigned[n] = 1 if (FlowsToAssing > 0) else 0
                FlowsToAssing -= 1
            Remainder = {}
            if FlowsToAssing >= 1:
                Quote = (B[0] + B[1]) / float(FlowsToAssing)
                for n in range(0, 2):
                    BitratePerQuote = B[n] / Quote
                    Floor = int(BitratePerQuote)
                    Remainder[n] = BitratePerQuote - Floor
                    NumberFlowsAssigned[n] += Floor
                    FlowsToAssing -= Floor
            else:
                    Remainder[0] = 0.0
                    Remainder[1] = 0.0

            R = sorted(Remainder.items(), key=operator.itemgetter(1), reverse=True)
            i = 0
            while (FlowsToAssing > 0):
                # Avoid gratuitous moves
                if Remainder[0] == Remainder[1]:
                    n = 0 if len_p0 >= len_p1 else 1
                else:
                    n = R[i][0]
                NumberFlowsAssigned[n] += 1
                FlowsToAssing -= 1
                i += 1

            if NumberFlowsAssigned[0] == len(path0_q):
                yield env.timeout(quantum)
                continue
            # Compute Balance and src dst
            if NumberFlowsAssigned[0] > len(path0_q):
                src = 1
                q_src = path1_q
                q_dst = path0_q
                to_move = NumberFlowsAssigned[0] - len_p0
            else:
                src = 0
                q_src = path0_q
                q_dst = path1_q
                to_move = NumberFlowsAssigned[1] - len_p1
            if to_move > 0:
                move_flow_list = sorted(q_src.keys()[0:to_move], reverse=True)
                try:
                    with paths_resource[0].request() as req:
                        yield req
                        if verbose:
                            print '%.4f Mon: Monitor going to interrupt path 0' % (env.now)
                        sending_q0_proc.interrupt()
                except:
                    pass
                try:
                    with paths_resource[1].request() as req:
                        yield req
                        if verbose:
                            print '%.4f Mon: Monitor going to interrupt path 1' % (env.now)
                        sending_q1_proc.interrupt()
                except:
                    pass
                yield env.timeout(0)
                removed = []
                for c in move_flow_list:
                    if verbose:
                        print ('%.4f: Mon: Removed flow %d from path %d' % (env.now, c, src))
                    q_dst[c] = q_src[c]
                    removed.append(c)
                    total_moved += 1
                    moved_key = str(total_moved) + '_' + str(c) + '_' + test_name + '_' + mode_name
                    rep_moved[moved_key] = [env.now, c, test_name, mode_name]
                for c in removed:
                    if verbose:
                        print 'Mon: going to remove flow %d from queue %d' % (c, src)
                    del q_src[c]
                # Restart
                with paths_resource[0].request() as req:
                    yield req
                    if verbose:
                        print '%4f Mon: Restarting sending_rr for path 0' % (env.now)
                        print path0_q
                    sending_q0_proc = env.process(sending_rr_flow(env, path0_q, PATH_SPEED_P0))
                with paths_resource[1].request() as req:
                    yield req
                    if verbose:
                        print '%4f Mon: Restarting sending_rr for path 1' % (env.now)
                        print path1_q
                    sending_q1_proc = env.process(sending_rr_flow(env, path1_q, PATH_SPEED_P1))
            else:
                pass
        yield env.timeout(quantum)  # Check every...

def flow_generator(env, paths, flow_interarrival, bytes_tosend, optimize=False, oper_mode=0, random_seed=-1):
    global verbose
    global debug_choose_p0
    global debug_choose_p1

    if random_seed >= 0:
        np.random.seed(random_seed)
        random.seed(random_seed)

    m = len(paths)
    if optimize and (m != 2):
        print 'Sorry, only can optimize for two paths'
        raise
    # r_start = random.randint(0, len(paths)-1)
    r_start = 0
    for c in range(0, len(flow_interarrival)):
        if (oper_mode == 0): # ECMP
            p = random.randint(0, len(paths)-1)
        elif (oper_mode == 1): # WCMP
            P0_affinity = PATH_SPEED_P0 / float(PATH_SPEED_P0 + PATH_SPEED_P1)
            p = 0 if (random.random() < P0_affinity) else 1
        elif (oper_mode == 2): # Bfast
            p = 0 if (PATH_SPEED_P0 >= PATH_SPEED_P1) else 1
        else: # Erlang
            p = (c + r_start) % m
        if (p == 0):
            debug_choose_p0 += 1
        else:
            debug_choose_p1 += 1

        yield env.timeout(flow_interarrival[c])
        try:
                env.process(isp_rr_flow(env, c, p, bytes_tosend[c], paths))
        except:
            print 'error calling isp_rr'
        if verbose:
            print '%.4f: flow %d selected path %d' % (env.now, c, p)

def flows_intervals(df_arg):
    df = df_arg.copy()
    df.sort_values(by=['ts', 'te'], inplace=True)
    df.reset_index(inplace=True, drop=True)
    left = df.ts[0]
    right = df.te[0]
    ival = pd.Interval(left=left, right=right, closed='both')
    working_list = []
    working_time = 0
    concurrent_ival = []
    concurrent_time = 0
    concur_ival = None
    concurrent_list = [right]
    max_concurrent = 1
    for i, row in df[1:].iterrows():
        if row.ts in ival:
            concurrent_list.append(row.te)
            concurrent_list = sorted(e for e in concurrent_list if e >= row.ts)
            concur_ival = pd.Interval(left=row.ts, right=min(row.te, ival.right), closed='both')
            concurrent_ival.append(concur_ival)
            new_end = max(ival.right, row.te)
            ival = pd.Interval(left=ival.left, right=new_end, closed='both')
        else:
            working_list.append(ival)
            working_time += ival.right - ival.left
            ival = pd.Interval(left=row.ts, right=row.te, closed='both')
            max_concurrent = max(len(concurrent_list), max_concurrent)
            concurrent_list = [row.te]
        max_concurrent = max(len(concurrent_list), max_concurrent)

    working_list.append(ival)
    working_time += ival.right - ival.left
    idle_list = []
    idle_duration = 0.0
    for i in range(0, len(working_list)-1):
        try:
            ival = pd.Interval(left=working_list[i].right, right=working_list[i+1].left, closed='neither')
            idle_list.append(ival)
        except:
            pass
        try:
            idle_duration += working_list[i+1].left - working_list[i].right
        except:
            pass

    # Merge concurrent_ival
    try:
        new_concurrent_ival = [concurrent_ival[0]]
    except:
        new_concurrent_ival = []
    for i in range(1, len(concurrent_ival)):
        last_ival =  new_concurrent_ival[-1:][0]
        try:
            if concurrent_ival[i].left in last_ival:
                new_ival = [pd.Interval(left=min(concurrent_ival[i].left,last_ival.left), right=max(concurrent_ival[i].right,last_ival.right), closed='both')]
                new_concurrent_ival = new_concurrent_ival[:-1] + new_ival
            else:
                new_concurrent_ival.append(concurrent_ival[i])
        except:
            print 'except', concurrent_ival[i]
            pass

    for i in range(0, len(new_concurrent_ival)):
        concurrent_time += new_concurrent_ival[i].right - new_concurrent_ival[i].left

    ret_dict = {}
    ret_dict['working_list'] = working_list
    ret_dict['working_time'] = working_time
    ret_dict['idle_list'] = idle_list
    ret_dict['idle_duration'] = idle_duration
    ret_dict['concurrent_ival'] = new_concurrent_ival
    ret_dict['concurrent_time'] = concurrent_time
    ret_dict['max_concurrent'] = max_concurrent
    return ret_dict

def run_simulation_manual_data(speed0, speed1, flow_arrival, bytes_tosend, optimize, oper_mode, number_paths, first_check, quantum, swap_time, is_verbose, show_results, flow_mode, sim_time, test_name='no_name', mode_name='no_mode', random_seed=0):
    global PATH_SPEED_P0
    global PATH_SPEED_P1
    global verbose
    global accR
    global accW
    global paths
    global finished_flows
    global max_finished
    global total_moved
    global rep
    global rep_all_flows
    global rep_moved
    global rep_moved_all_flows
    global tcam_rep_list_all

    flow_arrival = pd.Series(flow_arrival)
    bytes_tosend = pd.Series(bytes_tosend)

    accR = 0
    accW = 0
    finished_flows = 0
    max_finished = 0
    total_moved = 0
    rep = {}

    verbose = is_verbose
    flows = len(flow_arrival)

    env = simpy.Environment()
    path0 = simpy.Resource(env, 1)
    path1 = simpy.Resource(env, 1)

    # To avoid some sorts, we let p0 always with faster speed
    if (speed1 > speed0):
        temp = speed0
        speed0 = speed1
        speed1 = temp

    PATH_SPEED_P0 = speed0
    PATH_SPEED_P1 = speed1

    if number_paths == 1:
        paths = [path0]
    else:
        paths = [path0, path1]

    number_paths = len(paths)

    # Put in interarrival
    flow_interarrival = [ x - y for x, y in zip(flow_arrival[1:], flow_arrival)]
    flow_interarrival.insert(0,0)
    flow_interarrival_var = np.asarray(flow_interarrival).var()
    flow_interarrival_mean = np.asarray(flow_interarrival).mean()
    flows = len(flow_interarrival)

    if flow_mode:
        env.process(flow_generator(env, paths, flow_interarrival, bytes_tosend, optimize, oper_mode, random_seed))
        if optimize and number_paths > 1:
            env.process(REBALANCE(env, first_check, quantum, flows, paths, test_name, mode_name))
        if swap_time > 0 and number_paths > 1:
            env.process(swap_speed_flow(env, swap_time, paths, flows))
        env.run(until=sim_time)
    else:
        print 'removed'
        return

    if finished_flows < len(flow_interarrival):
        print 'Simulation incompleted, increase the sim_time'
        exit()

    mean_response = accR / float(flows)

    min_time = flow_arrival.min()
    fr = pd.DataFrame.from_dict(rep, orient='index', columns=['ts','te','sf'])
    fr['flow'] = fr.index
    rep = flows_intervals(fr)
    fr['r'] = fr.te - fr.ts
    fr['ts'] = flow_arrival
    fr['te'] = fr.ts + fr.r
    fr['test_name'] = test_name
    fr['mode_name'] = mode_name
    # Save all to TCAM impact
    if len(rep_all_flows) == 0:
        rep_all_flows = fr.copy()
    else:
        rep_all_flows = pd.concat([rep_all_flows, fr])
    # Save moved flows
    if len(rep_moved) > 0:
        for k, v in rep_moved.iteritems():
            v[0] += min_time
        if len(rep_moved_all_flows) == 0:
            rep_moved_all_flows = rep_moved
        else:
            rep_moved_all_flows.update(rep_moved)
        # Only moved flows
        rmoved = pd.DataFrame.from_dict(rep_moved, orient='index', columns=['tm','flow','test_name', 'mode_name'])
        rmoved = rmoved.set_index('flow')
        rmoved.index.names = ['index']
        rmoved = rmoved[['tm']]
        # Abs time
        fr['te'] = fr.te + min_time
        tcam_rep = pd.concat([rmoved, fr], axis=1, join='inner')
        tgb = tcam_rep.groupby(['flow', 'ts', 'test_name', 'mode_name'])
        tcam_rep_list = []
        for ti, gv in tgb:
            tm = gv.tm.min()
            te = gv.te.max()
            moves = len(gv)
            tlist = [tm, te, moves, ti[2], ti[3]]
            tcam_rep_list_all.append(tlist)

        rep_moved = {}

    net_throughput = float(sum(bytes_tosend)) / (max_finished - rep['idle_duration'])

    #####
    if show_results:
        print 'Finished Simulations at %.4f sec:' % max_finished
        print 'Amount of flows: %d' % (flows)
        print 'Amount of paths: %d' % (number_paths)
        print 'p0 speed %.4f Bps' % (PATH_SPEED_P0)
        if number_paths > 1:
            print 'p1 speed %.4f Bps' % (PATH_SPEED_P1)
        print 'Optimization: %s' % (optimize)
        print 'Flow Mode: %s' %(flow_mode)
        print 'Operation Mode: %d' % (oper_mode)

        print 'Workload finished at: %.4f sec and has moved %d flows' % (max_finished, total_moved)
        print 'Total Response time %.4f sec' % (accR)
        print 'Total Fuel sent %.4f l' % (sum(bytes_tosend))

        print '######### Real E[R]: %.4f sec ########' % (mean_response)
        print 'Net time: %.4f sec' % (max_finished-rep['idle_duration'])
        print 'Net Throughput %.4f Bps' % (net_throughput)
        print 'Gross Throughput %.4f Bps' % (float(sum(bytes_tosend)) / max_finished )

    return mean_response, net_throughput

def run_simulation_real_trace(files_dir, first_check, quantum, swap_time, is_verbose, show_results, flow_mode, output_file=False, random_seed=0):
    global rep_all_flows
    global rep_moved_all_flows
    global debug_choose_p0
    global debug_choose_p1
    global tcam_rep_list_all

    rep_all_flows = {}
    rep_moved_all_flows = {}
    tcam_rep_list_all = []

    batch_conf= files_dir + 'batch.conf'
    print batch_conf
    try:
        dfc = pd.read_csv(batch_conf, sep='|')
    except:
        print 'error opening batch_conf in run_simulation_real_trace'
        exit()

    sim_results = {}
    for i, row in dfc.iterrows():
        datafile = files_dir + row.filename
        throughput = float(row.throughput)
        mode_results = []
        mode_header = []

        # Configure the modes to run!
        modes = [6,7,9,10,11,12,14,15,16,17,19,20]

        # Configure the path rates
        # double # not selected for the paper
        s0 = max(1, int(math.ceil((throughput * 2. / 3.))))
        s1 = max(1, int(math.ceil((throughput * 1. / 3.))))
        # tenth
        s2 = max(1, int(math.ceil((throughput * 10. / 11.))))
        s3 = max(1, int(math.ceil((throughput * 1. / 11.))))
        # same
        s4 = max(1, int(math.ceil((throughput * 1. / 2.))))
        s5 = max(1, int(math.ceil((throughput * 1. / 2.))))
        # 3 times
        s6 = max(1, int(math.ceil((throughput * 3. / 4.))))
        s7 = max(1, int(math.ceil((throughput * 1. / 4.))))

        for m in modes:
            m = int(m)
            number_paths = 2
            # Speed selector
            if m in [1, 2, 3, 4, 5]:
                speed0 = s0
                speed1 = s1
            elif m in [6, 7, 8, 9, 10]:
                speed0 = s2
                speed1 = s3
            elif m in [11, 12, 13, 14, 15]:
                speed0 = s4
                speed1 = s5
            elif m in [16, 17, 18, 19, 20]:
                speed0 = s6
                speed1 = s7
            else:
                print 'not implemented'
                return

            # Rebalance Selector
            if m in [1,6,11,16,2,7,12,17]:
                optimize = False
            else:
                optimize = True

            # Default assignment selector
            if m in [1,6,11,16,4,9,14,19]:
                oper_mode = 0 # ECMP
            elif m in [2,7,12,17,3,8,13,18]:
                oper_mode = 1 # WCMP
            elif m in [5,10,15,20]:
                oper_mode = 2 # Bfast
            else:
                print 'mode not incompleted'
                exit()

            flows_info = pd.read_csv(datafile, delim_whitespace=True)
            flows_info.reset_index(inplace=True)
            # Let each flow starting with a very small difference time
            flows_info['start_time'] += flows_info['index'] / 1000000000.
            flow_arrival = flows_info.start_time
            bytes_tosend = flows_info.ibyt.astype(float)
            sim_time = max(1,(flows_info.ibyt.sum() / min(speed0+1, speed1+1)) * 1000.0)
            sim_time *= 20.
            test_name = row.filename
            mode_name = 'm' + str(m)
            result = run_simulation_manual_data(speed0, speed1, flow_arrival, bytes_tosend, optimize, oper_mode, number_paths, first_check, quantum, swap_time, is_verbose, show_results, flow_mode, sim_time, test_name, mode_name, random_seed)
            # Er
            try:
                result[0]
            except:
                print 'no result, check it'
                exit()

            mode_results.append(result[0])
            mode_header.append('m' + str(m))
            # net throughput
            mode_results.append(result[1])
            mode_header.append('t' + str(m))
            # Relative throughput
            mode_results.append(result[1] / (speed0 + speed1))
            mode_header.append('tr' + str(m))
            # Total Bytes
            mode_results.append(row.total_byt)
            mode_header.append('ibyt' + str(m))

        sim_results[row.filename] = mode_results
    if output_file:
        all_moved_output_file = output_file + '_all_moved.csv'
        print 'going to save', all_moved_output_file
        try:
            rep_moved_all_flows_df = pd.DataFrame.from_dict(rep_moved_all_flows, orient='index', columns=['tm','flow','test_name', 'mode_name'])
            rep_moved_all_flows_df.to_csv(all_moved_output_file, sep='|')
        except:
            print 'error printing', all_moved_output_file
            pass
        # TCAM impact
        all_tcam_impact_output_file = output_file + '_tcam_impact.csv'
        print 'going to save', all_tcam_impact_output_file
        try:
            tcam_rep_list_all_df = pd.DataFrame.from_records(tcam_rep_list_all, columns=['tm','te','moves','test_name','mode'])
            tcam_rep_list_all_df.to_csv(all_tcam_impact_output_file, sep='|')
        except:
            print 'error printing', all_tcam_impact_output_file
            pass
    return sim_results, mode_header

def call_simulator(files_dir, cpu, loadpercpu, q, fc):
    start_call = time.time()
    print 'quantum', q
    frames = []
    keys = []
    is_verbose = False
    # is_verbose = True
    show_results = False
    # show_results = True
    quantum = q
    first_check = fc
    flow_mode = True
    # flow_mode = False
    swap_time = 0.0 # Not used for these simulations
    max_seed = loadpercpu * (cpu)
    min_seed = max_seed - (loadpercpu)
    for seed in range(min_seed,max_seed):
        # Adjust here to change filenames
        version_name = 'INTNOR22'
        print 'seed', seed
        random_seed = seed
        # random_seed = 0 # Deixar zero para nao mudar seed
        # log_output_file = False
        log_output_file = files_dir + version_name + str(seed) + '_' + str(time.time())
        start_seed = time.time()
        sim_results, mode_header = run_simulation_real_trace(files_dir, first_check, quantum, swap_time, is_verbose, show_results, flow_mode, log_output_file, random_seed)
        end_seed = time.time()
        print 'duration:', end_seed - start_seed
        df = pd.DataFrame.from_dict(sim_results, orient='index', columns=mode_header)
        frames.append(df)
        keys.append('seed' + str(seed))
        output_file = files_dir + '/' + version_name + 'simflw-ecmp_wcmp_bfast_verify' + str(quantum) + '_first' + str(first_check) + '_swap' + str(swap_time) + '_seed' + str(min_seed) + '_' + str(max_seed-1) + '.csv'
    df_all = pd.concat(frames, keys=keys)
    df_all.to_csv(output_file, sep='|')
    end_call = time.time()
    print 'duration:', end_call - start_call
    return

##### TEST DO IMPLEMENT LRM
# debug_choose_p0 = 0
# debug_choose_p1 = 0
# rep_all_flows = {}
# rep_moved_all_flows = {}
# tcam_rep_list_all = []
# speed0 = 6.0
# speed1 = 2.0
# # flow_arrival = [0, 0, 1, 1, 2, 2]
# # bytes_tosend = [80, 80, 30, 30, 70, 70]
# # flow_arrival = [0, 0, 0, 0, 0, 0, 0]
# # bytes_tosend = [80, 80, 30, 30, 70, 70,100]
#
# flow_arrival = [0]
# bytes_tosend = [800]
#
# optimize = False
# optimize = True
# # bernoulli = False
# bernoulli = True
# number_paths = 2
# quantum = 0.1
# first_check = 0.001
# is_verbose = False
# is_verbose = True
# show_results = True
# flow_mode = True
# # flow_mode = False
# swap_time = 0.0
# sim_time = 1500000
# oper_mode = 0
# test_name = 'test_name'
# mode_name = 'mode_name'
# random_seed = 0
# run_simulation_manual_data(speed0, speed1, flow_arrival, bytes_tosend, optimize, oper_mode, number_paths, first_check, quantum, swap_time, is_verbose, show_results, flow_mode, sim_time, test_name, mode_name, random_seed)
# exit()
##### TEST DO IMPLEMENT LRM

# cpu = 1 # We have 8 cores
loadpercpu = 3 # 8x3 = 24 simulations
debug = False
# debug = True

debug_choose_p0 = 0
debug_choose_p1 = 0
lags = [0]
samplings = [16384, 4096, 1024]

if rnp:
    ows = [40, 20, 20]
else:
    ows = [40, 20, 20]

quanta = [20.0]
if rnp:
    base_dir = '../data/rnp_data/'
else:
    base_dir = '../data//wide_data/'

if debug:
    # ./simulator_batchs.py 1 1 | grep ER | awk 'BEGIN{acc=0;count=0} {acc+=$2;count+=1} END{print acc/count}'
    base_dir = './'
    samplings = [16384]
    ows = [40]
    lags = [0]
    quanta = [20.0]
    loadpercpu = 1

for lag in lags:
    for samp, ow in zip(samplings, ows):
        for q in quanta:
            print lag, samp, ow, q
            if rnp:
                work_dir = './Frnp_s' + str(samp) + '_ow' + str(ow) +  '/'
            else:
                work_dir = './Fwide_s' + str(samp) + '_ow' + str(ow) + '/'
            if debug:
                work_dir = './'
            files_dir = base_dir + work_dir
            print files_dir
            call_simulator(files_dir, cpu, loadpercpu, q, q/2.)
