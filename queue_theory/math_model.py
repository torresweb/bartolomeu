import math
import numpy as np

class MMmhat(object):
    def __init__(self, arrival, departures):
        myinfinity = 200
        self._departures  = np.asarray(departures)
        self._sum_capacity = self._departures.sum()
        self._capacity = len(departures)

        if self._sum_capacity <= arrival:
            raise ValueError("This Queue is unstable with the Input Parameters!!!")
        self._arrival = float(arrival)

        # P0
        preSum = 1
        numerator = 1.0
        denumerator = 1.0
        for i in xrange(1, self._capacity + 1):
            numerator *= self._arrival
            d = 0
            for j in xrange(0,i):
                d += self._departures[j]
                # print i, j, d
            denumerator *= d
            preSum += numerator / denumerator

        # Sum of capacities
        s_capacity = d
        last_denonimator = denumerator # Preservador pois todos os demais termos utilizam

        alpha = math.pow(self._arrival, self._capacity+1) / (math.pow(s_capacity, self._capacity+1) * last_denonimator)
        q = self._arrival / s_capacity
        preSuminfinity = (alpha / (1.0 - q)) * math.pow(s_capacity, self._capacity)
        preSum += preSuminfinity
        self._p0 = 1.0 / preSum
        # self._p0 = 0.5

        self._muhat = self.getMuHat()
        self._rhohat = self._arrival / self._muhat

    def getMuHat(self):
        numerator = 0.0
        denumerator = 0.0
        for i in range(1, self._capacity+1):
            p = self.getPk(i)
            numerator += self._departures[i-1] * p
            denumerator += p
        return numerator / denumerator

    def getPk(self, n):
        """
        Return the probability when there are `k` packets in the system
        """
        m = self._capacity
        if n == 0:
            return self._p0
        elif n <= m:
            numerator = 1
            denumerator = 1
            for i in range(1, n+1):
                numerator *= self._arrival
                d = 0
                for j in xrange(0,i):
                    d += self._departures[j]
                denumerator *= d
            return (numerator / denumerator) * self._p0
        else:
            numerator =  pow(self._arrival, self._capacity)
            numerator *= pow(self._arrival, n-m)
            denumerator = 1
            for i in range(1, m+1):
                d = 0
                for j in xrange(0,i):
                    d += self._departures[j]
                    # print i, j, d
                denumerator *= d
            for i in range(m, n):
                denumerator *= d
            return (numerator / denumerator) * self._p0

    def getQueueProb(self):
        """
        Return the probability when a packet comes, it needs to queue in the buffer.
        That is, P(W>0) = 1 - P(N < c)
        Also known as Erlang-C function
        """
        probSum = 0.0
        for i in range(0, self._capacity):
            probSum += self.getPk(i)
        return 1.0 - probSum

    def getAvgQueueTime(self):
        """
        Return the average time of packets spending in the queue
        """
        return self.getQueueProb() / (self._sum_capacity - self._arrival)

    def getAvgResponseTime(self):
        """
        Return the average time of packets spending in the system (in service and in the queue)
        """
        return self.getAvgQueueTime() + 1.0 / self._muhat

class MMcQueue(object):
    # ATENTION: This code was copied from:
    # https://github.com/HankerZheng/Basic-Calculator-for-Queueing-Theory
    # No license information was available.
    def __init__(self, arrival, departure, capacity):
        """
        Given the parameter of one M/M/c/c Queue,
        initialize the queue with these parameter and calculate some parameters.
        `_rou` :     Server Utilization
        `_p0`:      Probability of that there is no packets in the queue
        `_pc`:      Probability of that there is exactly `capacity` packets in the queue,
                    that is, all the server is busy.
        `_probSum`:  p0 + p1 + p2 + ... pc - pc
        `_finalTerm`: 1/(c!) * (arrival / departure)^c
        """
        if capacity * departure <= arrival:
            raise ValueError("This Queue is unstable with the Input Parameters!!!")
        self._arrival = float(arrival)
        self._departure = float(departure)
        self._capacity = capacity
        self._rou = self._arrival / self._departure / self._capacity

        # init the parameter as if the capacity == 0
        powerTerm = 1.0
        factorTerm = 1.0
        preSum = 1.0
        # Loop through `1` to `self._capacity` to get each term and preSum
        for i in xrange(1, self._capacity + 1):
            powerTerm *= self._arrival / self._departure
            factorTerm /= i
            preSum += powerTerm * factorTerm
        self._finalTerm = powerTerm * factorTerm
        preSum -= self._finalTerm
        self._p0 = 1.0 / (preSum + self._finalTerm / (1 - self._rou))
        self._pc = self._finalTerm * self._p0
        self._probSum = preSum * self._p0
        # print 'MMc', self._finalTerm, self._p0, self._pc, self._probSum


    @property
    def arrival(self):
        return self._arrival

    @property
    def departure(self):
        return self._departure

    @property
    def capacity(self):
        return self._capacity

    def getPk(self, k):
        """
        Return the probability when there are `k` packets in the system
        """
        if k == 0:
            return self._p0
        elif k == self._capacity:
            return self._pc
        elif k < self._capacity:
            factorTerm = 1.0 / math.factorial(k)
            powerTerm = math.pow(self._arrival / self._departure, k)
            return self._p0 * factorTerm * powerTerm
        else:
            return self._finalTerm * math.pow(self._rou, k - self._capacity) * self._p0

    def getQueueProb(self):
        """
        Return the probability when a packet comes, it needs to queue in the buffer.
        That is, P(W>0) = 1 - P(N < c)
        Also known as Erlang-C function
        """
        return 1.0 - self._probSum

    def getIdleProb(self):
        """
        Return the probability when the sever is idle.
        That is , P(N=0)
        """
        return self._p0

    def getAvgPackets(self):
        """
        Return the average number of packets in the system (in service and in the queue)
        """
        return self._rou / (1 - self._rou) * self.getQueueProb() + self._capacity * self._rou

    def getAvgQueueTime(self):
        """
        Return the average time of packets spending in the queue
        """
        return self.getQueueProb() / (self._capacity * self._departure - self._arrival)

    def getAvgQueuePacket_Given(self):
        """
        Given there is packet in the queue,
        return the average number of packets in the queue
        """
        return self._finalTerm * self._p0 / (1.0 - self._rou) / (1.0 - self._rou)

    def getAvgQueueTime_Given(self):
        """
        Given a packet must wait,
        return the average time of this packet spending in the queue
        """
        if self.getQueueProb() == 0:
            return 0
        return self.getAvgQueuePacket_Given() / (self.getQueueProb() * self._arrival)

    def getAvgResponseTime(self):
        """
        Return the average time of packets spending in the system (in service and in the queue)
        """
        return self.getAvgQueueTime() + 1.0 / self._departure

    def getAvgPacketInSystem(self):
        """
        Return the average number of packets in the system.
        """
        return self.getAvgResponseTime() * self._arrival

    def getAvgBusyServer(self):
        """
        Return the average number of busy Server.
        """
        return self.arrival / self.departure


    def getPorbWhenQueueTimeLargerThan(self, queueTime):
        """
        Return the probability when the queuing time of the packet is larger than `queueTime`
        That is P(W > queueTime) = 1 - P(W <= queueTime)
        """
        firstTerm = self._pc / (1.0 - self._rou)
        expTerm = - self._capacity * self._departure * (1.0 - self._rou) * queueTime
        secondTerm = math.exp(expTerm)
        return firstTerm * secondTerm

if __name__ == '__main__':

    la = 0.5
    m = 1.0
    mhat=[m]
    mhat=[m,m]
    # mhat=[m,m,m]
    # mhat = [1.5, 1.5]
    # mhat = [1.5, 0.75]
    # mhat = [3.0, 3.0]
    # mhat = [3.0, 1.5]

    k = 0
    print '############# MMc'
    print MMcQueue(la,m,len(mhat)).getPk(k)
    print MMcQueue(la,m,len(mhat)).getQueueProb()
    print MMcQueue(la,m,len(mhat)).getAvgQueueTime()
    print MMcQueue(la,m,len(mhat)).getAvgResponseTime()
    print '############# MMmhat'
    print MMmhat(la,mhat).getPk(k)
    print MMmhat(la,mhat).getQueueProb()
    print MMmhat(la,mhat).getAvgQueueTime()
    print MMmhat(la,mhat).getAvgResponseTime()


    # Bernoulli diferente capacities
    soujour_bernoulli = 0
    services = len(mhat)
    for i in range(0, services):
        la_i = la / services
        soujour_bernoulli += (la_i / la) * (1.0/mhat[i] + MMcQueue(la_i, mhat[i], 1).getAvgQueueTime())
    print 'soujour_bernoulli', soujour_bernoulli
